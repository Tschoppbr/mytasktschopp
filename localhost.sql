-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `task` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `task`;

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `due_at` datetime NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `priority` tinyint(1) NOT NULL DEFAULT '1',
  `status` enum('open','close') CHARACTER SET utf8 NOT NULL DEFAULT 'open',
  `done_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `task` (`id`, `description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `priority`, `status`, `done_by`) VALUES
(2,	'Louer une voiture pour les vacs',	'2017-06-25 19:23:06',	1,	'2017-06-25 19:23:06',	5,	2,	'close',	1),
(1,	'Finir ce mytask une bonne fois pour toute',	'2017-06-25 19:21:33',	1,	'2017-06-25 19:21:33',	1,	1,	'open',	NULL),
(4,	'Ammener mon scoot au garage',	'2017-06-25 19:31:58',	1,	'2017-06-29 00:00:00',	1,	2,	'open',	NULL),
(5,	'Paier la facture',	'2017-06-25 23:53:25',	1,	'2017-06-28 00:00:00',	3,	3,	'open',	NULL);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `username`, `email`, `password`) VALUES
(1,	'Bruno',	'tschoppbr@gmail.com',	'gugu'),
(2,	'mickaaa',	'muscle@gmail.com',	'protein'),
(3,	'seb',	'seb@gmail.com',	'bulle'),
(4,	'maxou',	'maximal@gmail.com',	'maximum'),
(5,	'fefe',	'fefecajeux@gmail.com',	'barbu');

-- 2017-06-26 08:22:55
